package com.e.testapplication.ble

import android.bluetooth.*
import android.util.Log
import com.e.testapplication.core.TestApplication
import javax.inject.Inject

class BleConnectionManager @Inject constructor(
    var context: TestApplication,
    var bleScanner: BleScanner
) {

    private lateinit var currentDeviceUuid: String
    private lateinit var connectionsCallback: (result: ConnectionState) -> Unit

    private var gatt: BluetoothGatt? = null
    private var bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private var gattCallback: GattCallback

    init {
        gattCallback = GattCallback()
    }

    fun connect(deviceUUID: String, connectionsCallback: (result: ConnectionState) -> Unit) {
        Log.d(TAG, "***connect: ")
        currentDeviceUuid = deviceUUID
        this.connectionsCallback = connectionsCallback
        connectToRemoteDevice()
    }

    private fun connectToRemoteDevice() {
        setConnectionState(ConnectionState.CONNECTING)
        gatt = bluetoothAdapter
            .getRemoteDevice(currentDeviceUuid)
            .connectGatt(context, false, gattCallback)
    }

    private fun startSearchDevice() {
        bleScanner.searchDeviceByUUID(currentDeviceUuid, callback = { connectToRemoteDevice() })
    }

    fun disconnect() {
        gatt?.disconnect()
        gatt?.close()
        gatt = null
    }

    fun setConnectionState(connectionState: ConnectionState) {
        connectionsCallback.invoke(connectionState)
        when (connectionState) {
            ConnectionState.SEARCHING -> startSearchDevice()
        }
    }

    private inner class GattCallback : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            Log.d(TAG, "***onConnectionStateChange status: " + status + "  newState: " + newState)
            if (status != BluetoothGatt.GATT_SUCCESS) {
                setConnectionState(ConnectionState.SEARCHING)
                return
            }
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> setConnectionState(ConnectionState.CONNECTED)
                BluetoothProfile.STATE_DISCONNECTED -> setConnectionState(ConnectionState.DISCONNECTED)
                else -> setConnectionState(ConnectionState.DISCONNECTED)
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            Log.d(TAG, "***onServicesDiscovered: " + status)
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            val uuid = characteristic?.uuid
            val value = characteristic?.value
            Log.d(TAG, "***onCharacteristicRead uuid: " + uuid + "  value: " + value.toString())
        }

        override fun onCharacteristicWrite(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            Log.d(TAG, "***onCharacteristicWrite: " + (characteristic?.uuid))
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?
        ) {
            Log.d(TAG, "***onCharacteristicChanged: " + characteristic?.uuid)
        }

        override fun onDescriptorRead(
            gatt: BluetoothGatt?,
            descriptor: BluetoothGattDescriptor?,
            status: Int
        ) {
            Log.d(TAG, "***onDescriptorRead: ")
        }

        override fun onDescriptorWrite(
            gatt: BluetoothGatt?,
            descriptor: BluetoothGattDescriptor?,
            status: Int
        ) {
            Log.d(TAG, "***onDescriptorWrite: ")
        }

        override fun onReadRemoteRssi(gatt: BluetoothGatt?, rssi: Int, status: Int) {
            Log.d(TAG, "***onReadRemoteRssi: " + rssi)
        }
    }

    companion object {
        var TAG: String = "BleConnectionProvider"
    }
}