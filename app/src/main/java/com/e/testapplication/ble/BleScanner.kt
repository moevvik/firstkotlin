package com.e.testapplication.ble

import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.*
import android.util.Log

class BleScanner {

    private val defaultAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private val bluetoothLeScanner: BluetoothLeScanner
    private lateinit var callback: (result: ScanResult) -> Unit

    init {
        bluetoothLeScanner = defaultAdapter.bluetoothLeScanner
    }

    fun startScanning(callback: (result: ScanResult) -> Unit) {
        Log.d(TAG, "***startScanning: ")
        this.callback = callback
        startScan(arrayListOf())
    }

    fun searchDeviceByUUID(deviceUuid: String, callback: (result: ScanResult) -> Unit) {
        Log.d(TAG, "***searchDeviceByUUID: " + deviceUuid)
        this.callback = callback
        val filters: MutableList<ScanFilter> =
            arrayListOf(ScanFilter.Builder().setDeviceAddress(deviceUuid).build())
        startScan(filters)
    }

    private fun startScan(filters: MutableList<ScanFilter>) {
        if (defaultAdapter.isEnabled.not()) {
            defaultAdapter.enable()
            return
        }

        bluetoothLeScanner.startScan(
            filters,
            ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
                .setNumOfMatches(ScanSettings.MATCH_NUM_ONE_ADVERTISEMENT)
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .build(),
            bleCallback
        )
    }

    fun stopScanning() {
        Log.d(TAG, "***stopScanning: ")
        bluetoothLeScanner.stopScan(bleCallback)
    }

    private val bleCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            Log.d(TAG, "***onScanResult: " + result)
            result?.let { callback.invoke(it) }
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            results?.forEach {
            }
        }
    }

    companion object {
        val TAG: String = "BleManager"
    }
}