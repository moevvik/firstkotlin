package com.e.testapplication.ble

enum class ConnectionState {
    CONNECTED, DISCONNECTED, SEARCHING, CONNECTING
}