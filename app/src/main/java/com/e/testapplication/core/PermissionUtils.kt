package com.e.testapplication.core

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment

class PermissionUtils {

    val permissionsToRequest = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION)

    fun checkLocationPermission(activity: Activity): Boolean {
        return checkPermissions(activity, permissionsToRequest)
    }

    fun locationPermissionRequest(fragment: Fragment, requestCode: Int) {
        fragment.requestPermissions(permissionsToRequest, requestCode)
    }

    fun checkPermissions(activity: Activity, permissions: Array<String>): Boolean {
        val notGrantedPermissions =
            permissions.filterNot { activity.checkSelfPermission(it) == PackageManager.PERMISSION_GRANTED }

        return notGrantedPermissions.isEmpty()
    }

    fun requestPermissions(activity: Activity, permissions: Array<String>, requestCode: Int) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode)
    }
}