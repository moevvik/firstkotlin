package com.e.testapplication.di

import com.e.testapplication.core.TestApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@Component(
    modules = arrayOf(
        AndroidSupportInjectionModule::class,
        AppModule::class,
        BuildersModule::class
    )
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: TestApplication): Builder
        fun build(): AppComponent
    }

    fun inject(application: TestApplication)
}