package com.e.testapplication.di

import android.content.Context
import com.e.testapplication.ble.BleConnectionManager
import com.e.testapplication.ble.BleScanner
import com.e.testapplication.core.PermissionUtils
import com.e.testapplication.core.TestApplication
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideContext(application: TestApplication): Context {
        return application.applicationContext
    }

    @Provides
    fun provideBleLauncher(): BleScanner {
        return BleScanner()
    }

    @Provides
    fun provideBleConnectionProvider(
        application: TestApplication,
        bleScanner: BleScanner
    ): BleConnectionManager {
        return BleConnectionManager(application, bleScanner)
    }

    @Provides
    fun providePermissionUtils(): PermissionUtils {
        return PermissionUtils()
    }
}