package com.e.testapplication.di

import com.e.testapplication.ui.fragments.ble_device.BleDeviceFragment
import com.e.testapplication.ui.fragments.ble_device.BleDeviceFragmentModule
import com.e.testapplication.ui.fragments.ble_scanner.BleScannerFragment
import com.e.testapplication.ui.fragments.ble_scanner.BleScannerFragmentModule
import com.e.testapplication.ui.fragments.main.MainFragment
import com.e.testapplication.ui.fragments.main.MainFragmentModule
import com.e.testapplication.ui.main.MainActivity
import com.e.testapplication.ui.main.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector(modules = arrayOf(MainModule::class))
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector(modules = arrayOf(MainFragmentModule::class))
    abstract fun mainFragment(): MainFragment

    @ContributesAndroidInjector(modules = arrayOf(BleScannerFragmentModule::class))
    abstract fun bleScannerFragment(): BleScannerFragment

    @ContributesAndroidInjector(modules = arrayOf(BleDeviceFragmentModule::class))
    abstract fun bleDeviceFragment(): BleDeviceFragment
}