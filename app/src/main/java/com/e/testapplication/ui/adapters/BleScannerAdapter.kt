package com.e.testapplication.ui.adapters

import android.bluetooth.le.ScanResult
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.testapplication.R

class BleScannerAdapter() : RecyclerView.Adapter<BleScannerAdapter.VHolder>() {

    private lateinit var callback: (uuud: String) -> Unit
    private var deviceList = ArrayList<ScanResult>()

    constructor(callback: (uuud: String) -> Unit) : this() {
        this.callback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHolder =
        VHolder(LayoutInflater.from(parent.context), parent)

    override fun onBindViewHolder(holder: VHolder, position: Int) {
        val scanResult = deviceList[holder.adapterPosition]
        holder.bind(scanResult)
        holder.itemView.setOnClickListener { callback.invoke(scanResult.device.address) }
    }

    override fun getItemCount(): Int = deviceList.size

    fun setData(newList: MutableList<ScanResult>) {
        val scanResultDiffUtilsCallback = ScanResultDiffUtilsCallback(deviceList, newList)
        val diffResult = DiffUtil.calculateDiff(scanResultDiffUtilsCallback)

        deviceList.clear()
        deviceList.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    class VHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.adapter_ble_scanner, parent, false)) {

        private var titleTv: TextView?
        private var uuidTv: TextView?
        private var rssiTv: TextView?

        init {
            titleTv = itemView.findViewById(R.id.titleTv)
            uuidTv = itemView.findViewById(R.id.uuidTv)
            rssiTv = itemView.findViewById(R.id.rssiTv)
        }

        fun bind(scanResult: ScanResult) {
            titleTv?.text = scanResult.scanRecord?.deviceName
            uuidTv?.text = scanResult.device.address
            rssiTv?.text = scanResult.rssi.toString()
        }
    }
}