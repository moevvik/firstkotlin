package com.e.testapplication.ui.adapters

import android.bluetooth.le.ScanResult
import androidx.recyclerview.widget.DiffUtil

class ScanResultDiffUtilsCallback(
    private var oldList: MutableList<ScanResult>,
    private var newList: MutableList<ScanResult>
) : DiffUtil.Callback() {


    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].device.address.equals(newList[newItemPosition].device.address)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].rssi == newList[newItemPosition].rssi
    }
}