package com.e.testapplication.ui.fragments.ble_device

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.e.testapplication.R
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_ble_device.*
import javax.inject.Inject

class BleDeviceFragment : Fragment(), BleDeviceFragmentContract.View {

    companion object {
        var DEVICE_UUID_BUNDLE: String = "DEVICE_UUID_BUNDLE"
        val TAG: String = "BleDeviceFragment"

        fun getBundle(deviceUuid: String): Bundle {
            val bundle = Bundle()
            bundle.putString(DEVICE_UUID_BUNDLE, deviceUuid)
            return bundle
        }
    }

    @Inject
    lateinit var presenter: BleDeviceFragmentContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ble_device, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)

        val deviseUUid = arguments?.getString(DEVICE_UUID_BUNDLE)
        uuidTv.text = deviseUUid

        if (deviseUUid != null) {
            presenter.connectToDevice(deviseUUid)
        }
    }

    override fun onDestroy() {
        presenter.disconnectDevice()
        super.onDestroy()
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun updateConnectionState(connectionState: String) {
        Log.d(TAG, "***updateConnectionState text: " + connectionState)
        activity?.runOnUiThread {
            connectionStatusTv?.text = connectionState
        }
    }

    override fun updateNameTitle(deviceName: String) {
        nameTv.text = deviceName
    }
}