package com.e.testapplication.ui.fragments.ble_device

import com.e.testapplication.core.BaseContract

interface BleDeviceFragmentContract {

    interface View : BaseContract.View {
        fun updateConnectionState(connectionState: String)
        fun updateNameTitle(deviceName: String)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun connectToDevice(deviceUuid: String)
        fun disconnectDevice()
    }
}