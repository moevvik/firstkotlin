package com.e.testapplication.ui.fragments.ble_device

import dagger.Binds
import dagger.Module

@Module
abstract class BleDeviceFragmentModule {

    @Binds
    abstract fun providePresenter(bleDeviceFragmentPresenter: BleDeviceFragmentPresenter): BleDeviceFragmentContract.Presenter
}