package com.e.testapplication.ui.fragments.ble_device

import android.util.Log
import com.e.testapplication.ble.BleConnectionManager
import com.e.testapplication.ble.ConnectionState
import javax.inject.Inject

class BleDeviceFragmentPresenter @Inject constructor() : BleDeviceFragmentContract.Presenter {

    lateinit var view: BleDeviceFragmentContract.View

//    val connectionStatus = MutableLiveData<ConnectionState>()

    @Inject
    lateinit var bleConnectionManager: BleConnectionManager

    override fun connectToDevice(deviceUuid: String) {
        bleConnectionManager.connect(deviceUuid, this::onConnectionStateChanged)
    }

    override fun disconnectDevice() {
        bleConnectionManager.disconnect()
    }

    private fun onConnectionStateChanged(connectionState: ConnectionState) {
        Log.d("BleDevPres", "***onConnectionStateChanged: " + connectionState)

        view.updateConnectionState(connectionState.toString())
    }

    override fun subscribe() {

    }

    override fun unsubscribe() {

    }

    override fun attach(view: BleDeviceFragmentContract.View) {
        this.view = view
    }
}