package com.e.testapplication.ui.fragments.ble_scanner

import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.e.testapplication.R
import com.e.testapplication.core.PermissionUtils
import com.e.testapplication.ui.adapters.BleScannerAdapter
import com.e.testapplication.ui.fragments.ble_device.BleDeviceFragment
import com.e.testapplication.ui.fragments.main.MainFragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_ble_scanner.*
import javax.inject.Inject

class BleScannerFragment : Fragment(), BleScannerFragmentContract.View {

    companion object {
        val TAG: String = "BleScannerFragment"
    }

    @Inject
    lateinit var presenter: BleScannerFragmentContract.Presenter

    @Inject
    lateinit var permissionUtils: PermissionUtils

    lateinit var bleScannerAdapter: BleScannerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_ble_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)

        scanBtn.setOnClickListener { presenter.onStartBleScanClick() }
        stopScanBtn.apply {
            isEnabled = false
            setOnClickListener { presenter.onStopBleScanClick() }
        }

        bleScannerAdapter = BleScannerAdapter(presenter::onDeviceSelected)
        recycleView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = bleScannerAdapter
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun isPermissionGranted(): Boolean {
        return permissionUtils.checkLocationPermission(requireActivity())
    }

    override fun permissionRequest() {
        permissionUtils.locationPermissionRequest(this, 100)
    }

    override fun updateItemList(itemList: MutableList<ScanResult>) {
        bleScannerAdapter.setData(itemList)
        itemList.forEach { println(it.rssi) }
    }

    override fun updateScanBtnStatus(enable: Boolean) {
        scanBtn.isEnabled = enable
    }

    override fun updateStopScanBtnStatus(enable: Boolean) {
        stopScanBtn.isEnabled = enable
    }

    override fun openBleDeviceFragment(deviceUuid: String) {
        findNavController().navigate(
            R.id.bleDeviceFragment,
            BleDeviceFragment.getBundle(deviceUuid)
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d(MainFragment.TAG, "***onRequestPermissionsResult: ")
        if (grantResults.contains(PackageManager.PERMISSION_DENIED)) {
            permissionRequest()
            return
        }

        presenter.onStartBleScanClick()
    }
}