package com.e.testapplication.ui.fragments.ble_scanner

import android.bluetooth.le.ScanResult
import com.e.testapplication.core.BaseContract

interface BleScannerFragmentContract {
    interface View : BaseContract.View {
        fun isPermissionGranted(): Boolean
        fun permissionRequest()
        fun updateItemList(itemList: MutableList<ScanResult>)
        fun updateScanBtnStatus(enable: Boolean)
        fun updateStopScanBtnStatus(enable: Boolean)
        fun openBleDeviceFragment(deviceUuid: String)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun onStartBleScanClick()
        fun onStopBleScanClick()
        fun onDeviceSelected(deviceUuid: String)
    }
}