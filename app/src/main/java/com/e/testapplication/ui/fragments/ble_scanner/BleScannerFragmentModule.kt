package com.e.testapplication.ui.fragments.ble_scanner

import dagger.Binds
import dagger.Module

@Module
abstract class BleScannerFragmentModule {

    @Binds
    abstract fun providePresenter(bleScannerFragmentPresenter: BleScannerFragmentPresenter): BleScannerFragmentContract.Presenter
}