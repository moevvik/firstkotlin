package com.e.testapplication.ui.fragments.ble_scanner

import android.bluetooth.le.ScanResult
import android.util.Log
import com.e.testapplication.ble.BleScanner
import javax.inject.Inject

class BleScannerFragmentPresenter @Inject constructor() : BleScannerFragmentContract.Presenter {

    lateinit var view: BleScannerFragmentContract.View

    @Inject
    lateinit var bleScanner: BleScanner

    private var deviceList = mutableListOf<ScanResult>()

    override fun onStartBleScanClick() {
        if (view.isPermissionGranted()) {
            startBleScan()
            return
        }
        view.permissionRequest()
    }

    override fun onStopBleScanClick() {
        stopBleScan()
    }

    override fun onDeviceSelected(deviceUuid: String) {
        stopBleScan()
        view.openBleDeviceFragment(deviceUuid)
    }

    private fun stopBleScan() {
        bleScanner.stopScanning()
        view.updateScanBtnStatus(true)
        view.updateStopScanBtnStatus(false)
    }

    private fun startBleScan() {
        view.updateScanBtnStatus(false)
        view.updateStopScanBtnStatus(true)
        deviceList.clear()
        bleScanner.startScanning(this::onDeviceFound)
    }

    private fun onDeviceFound(scanResult: ScanResult) {
        Log.d("BleFrPres", "***onDeviceFound: " + scanResult.device.name)
        val find = deviceList.find { it.device.address == scanResult.device.address }
        if (find != null) {
            val indexOf = deviceList.indexOf(find)
            deviceList[indexOf] = scanResult
            view.updateItemList(deviceList.sortedWith(devicesComparator) as MutableList<ScanResult>)
            return
        }

        deviceList.add(scanResult)
        view.updateItemList(deviceList.sortedWith(devicesComparator) as MutableList<ScanResult>)
    }

    object devicesComparator : Comparator<ScanResult> {
        override fun compare(o1: ScanResult?, o2: ScanResult?): Int =
            o2!!.rssi.compareTo(o1!!.rssi)
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
    }

    override fun attach(view: BleScannerFragmentContract.View) {
        this.view = view
    }
}