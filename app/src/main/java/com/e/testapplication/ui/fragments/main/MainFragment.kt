package com.e.testapplication.ui.fragments.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.e.testapplication.R
import com.e.testapplication.core.PermissionUtils
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

class MainFragment : Fragment(),
    MainFragmentContract.View {

    companion object {
        val TAG: String = "MainFragment"
    }

    @Inject
    lateinit var presenter: MainFragmentContract.Presenter

    @Inject
    lateinit var permissionUtils: PermissionUtils

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attach(this)

        scanBtn.setOnClickListener { presenter.onBleScanClick() }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun updateFilterValue(value: String) {
        filterValue.text = value
    }

    override fun openBleScannerFragment() {
        findNavController().navigate(R.id.bleScannerFragment)
    }

}