package com.e.testapplication.ui.fragments.main

import com.e.testapplication.core.BaseContract


interface MainFragmentContract {

    interface View : BaseContract.View {
        fun updateFilterValue(value: String)
        fun openBleScannerFragment()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun onBleScanClick()
    }
}