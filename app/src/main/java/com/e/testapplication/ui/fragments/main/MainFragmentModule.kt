package com.e.testapplication.ui.fragments.main

import dagger.Binds
import dagger.Module

@Module
abstract class MainFragmentModule {

    @Binds
    abstract fun bindMainFragmentPresenter(mainFragmentPresenter: MainFragmentPresenter): MainFragmentContract.Presenter
}