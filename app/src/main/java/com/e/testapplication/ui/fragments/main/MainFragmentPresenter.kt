package com.e.testapplication.ui.fragments.main

import android.content.Context
import androidx.preference.PreferenceManager
import javax.inject.Inject

class MainFragmentPresenter @Inject constructor() : MainFragmentContract.Presenter {

    lateinit var view: MainFragmentContract.View

    @Inject
    lateinit var context: Context

    override fun onBleScanClick() {
        view.openBleScannerFragment()
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
    }

    override fun attach(view: MainFragmentContract.View) {
        this.view = view
        loadData()
    }

    fun loadData() {
        val settingsSP = PreferenceManager.getDefaultSharedPreferences(context)
        settingsSP.getString("filter_name", "NA")?.let { view.updateFilterValue(it) }
    }
}