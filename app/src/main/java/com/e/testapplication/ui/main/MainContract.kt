package com.e.testapplication.ui.main

import com.e.testapplication.core.BaseContract

interface MainContract : BaseContract {

    interface View : BaseContract.View {
        fun showSettings()
    }

    interface Presenter :
        BaseContract.Presenter<View> {
        fun settingsClick()
    }
}