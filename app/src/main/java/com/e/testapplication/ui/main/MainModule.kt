package com.e.testapplication.ui.main

import dagger.Binds
import dagger.Module

@Module
abstract class MainModule {
    @Binds
    abstract fun bindMainPresenter(mainPresenter: MainPresenter): MainContract.Presenter
}