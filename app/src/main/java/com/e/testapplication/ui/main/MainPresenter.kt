package com.e.testapplication.ui.main

import javax.inject.Inject

class MainPresenter @Inject constructor() : MainContract.Presenter {

    private lateinit var view: MainContract.View

    override fun settingsClick() {
        view.showSettings()
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
    }

    override fun attach(view: MainContract.View) {
        this.view = view
    }
}